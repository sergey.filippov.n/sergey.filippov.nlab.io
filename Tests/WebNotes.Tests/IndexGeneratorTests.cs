﻿namespace Tests
{
    using System;

    using NUnit.Framework;

    using WebNotes.Document.Model;
    using WebNotes.StaticIndexGenerator;

    [TestFixture]
    public class IndexGeneratorTests
    {
        [Test]
        public void TestGeneration()
        {
            var basicCollection = new[]
                                      {
                                          new DocumentEntity()
                                              {
                                                  Metadata = new Metadata()
                                                                 {
                                                                     Name = "Test",
                                                                     Author = "me",
                                                                     Date = DateTime.Now,
                                                                     DocumentType = DocumentTypeEnum.BlogEntry,
                                                                 },
                                                  RelativePath = "Documents/First.md"
                                              },
                                          new DocumentEntity()
                                              {
                                                  Metadata = new Metadata()
                                                                 {
                                                                     Name = "Test2",
                                                                     Author = "me",
                                                                     Date = DateTime.Now + TimeSpan.FromDays(1),
                                                                     DocumentType = DocumentTypeEnum.AppEntry,
                                                                 },
                                                  RelativePath = "Documents/Second.md"
                                              },
                                      };

            var builder = new DocumentIndexBuilder(basicCollection);
            var result = builder.DocumentIndex;
            Console.Write(result.ToString());
            //Assert.Pass(result.ToString());
        }
    }
}