﻿namespace Tests
{
    using NUnit.Framework;

    using WebNotes.Document.Domain;

    public class DocumentReaderTest
    {
        private string testString = @"@{DocumentType: BlogEntry
Name: The first entry.
Author: Sergey Filippov
Date: 2019-08-18}@

## The first entry ever
Welcome to this epic moment. The first indexed text entry to be reached and read by a pure client side Blazor web app.

<br/>

### Why is it important
This is very special moment, since this will prove the concept of purely client side web app, that has very low requirements for a server to host it.";

        [Test]
        public void Parse()
        {
            var reader = new DocumentReader();
            var result = reader.Read(testString);
            Assert.IsTrue(result.Metadata != null);
        }
    }
}