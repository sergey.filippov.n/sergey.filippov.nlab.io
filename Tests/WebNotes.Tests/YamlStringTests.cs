using System;
using NUnit.Framework;
using WebNotes.Document.Model;
using YamlDotNet.Serialization;

namespace Tests
{
    [TestFixture]
    public class YamlStringTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestMetadataSerialization()
        {
            var metadata = new Metadata()
                {Name = "The firts entry.", Date = DateTime.UtcNow, Author = "Sergey Filippov", DocumentType = DocumentTypeEnum.BlogEntry};

            var serializer = new SerializerBuilder().EmitDefaults().Build();
            var result = serializer.Serialize(metadata);

            Console.Write(result);
        }

        [Test]
        public void TestMetadataDeserialization()
        {
            var input = @"DocumentType: BlogEntry
Name: The firts entry.
Author: Sergey Filippov
Date: 2019-08-18";

            var deserializer = new DeserializerBuilder().Build();
            var result = deserializer.Deserialize<Metadata>(input);

            Assert.AreEqual(DocumentTypeEnum.BlogEntry, result.DocumentType);
            Assert.AreEqual("Sergey Filippov", result.Author);
        }

        [Test]
        public void TestDocumentSerialization()
        {
            var metadata = new Metadata()
                { Name = "The firts entry.", Date = DateTime.UtcNow, Author = "Sergey Filippov", DocumentType = DocumentTypeEnum.BlogEntry };
            var document = new DocumentEntity() {Metadata = metadata};
            document.Content = @"asdasda asd asd asd asd asdasdsa dsad asd asd asdsad 
asdasda asd asd asd asd asdasdsa dsad asd asd asdsad 
asdasda asd asd asd asd asdasdsa dsad asd asd asdsad 
asdasda asd asd asd asd asdasdsa dsad asd asd asdsad 
asdasda asd asd asd asd asdasdsa dsad asd asd asdsad 
asdasda asd asd asd asd asdasdsa dsad asd asd asdsad 
asdasda asd asd asd asd asdasdsa dsad asd asd asdsad 
asdasda asd asd asd asd asdasdsa dsad asd asd asdsad 
asdasda asd asd asd asd asdasdsa dsad asd asd asdsad 
";
            var serializer = new SerializerBuilder().EmitDefaults().Build();
            var result = serializer.Serialize(serializer);
            Console.Write(result);
        }
        
    }
}