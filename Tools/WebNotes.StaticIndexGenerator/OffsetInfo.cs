﻿#nullable enable
namespace WebNotes.StaticIndexGenerator
{
    public class OffsetInfo
    {
        private int offsetNumber;

        public OffsetInfo()
        {
            this.Offset = string.Empty;
        }

        public int OffsetNumber
        {
            get => this.offsetNumber;
            set
            {
                this.offsetNumber = value;
                if (value == 0)
                {
                    this.Offset = string.Empty;
                }
                else
                {
                    this.Offset = new string(' ', value * 4);
                }
            }
        }

        public string Offset { get; private set; }

        public override string? ToString()
        {
            return this.Offset;
        }
    }
}