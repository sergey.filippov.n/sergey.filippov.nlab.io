﻿namespace WebNotes.StaticIndexGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using WebNotes.Document.Model;

    /// <summary>
    /// The document index builder.
    /// </summary>
    public class DocumentIndexBuilder
    {
        /// <summary>
        /// The string builder.
        /// </summary>
        private readonly StringBuilder stringBuilder = new StringBuilder();

        /// <summary>
        /// The offset.
        /// </summary>
        private readonly OffsetInfo offset = new OffsetInfo();

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentIndexBuilder"/> class.
        /// </summary>
        /// <param name="documents">
        /// The documents.
        /// </param>
        public DocumentIndexBuilder(IEnumerable<DocumentEntity> documents)
        {
            this.AppendWithOffset(@"namespace WebNotesView.Generated");
            this.AppendWithOffset(@"{");
            this.offset.OffsetNumber++;

            this.AppendWithOffset("using System;");
            this.AppendWithOffset("using WebNotes.Document.Model;");
            this.AppendWithOffset();

            this.AppendWithOffset(@"/// <summary>");
            this.AppendWithOffset(@"/// The document index.");
            this.AppendWithOffset(@"/// </summary>");
            this.AppendWithOffset(@"public class DocumentIndex");
            this.AppendWithOffset(@"{");
            this.offset.OffsetNumber++;

            this.AppendWithOffset(@"/// <summary>");
            this.AppendWithOffset(@"/// The metadata index.");
            this.AppendWithOffset(@"/// </summary>");
            this.AppendWithOffset(@"public readonly IIndexMetadata[] MetadataIndex =");
            this.AppendWithOffset(@"{");
            this.offset.OffsetNumber++;

            foreach (var document in documents)
            {
                var meta = document.Metadata;
                this.AppendWithOffset(
                    $"new IndexMetadata {{ Name = \"{meta.Name}\""
                        + $", Author = \"{meta.Author}\""
                        + $", Date = DateTime.ParseExact(\"{meta.Date:yyyy-MM-dd}\", \"yyyy-MM-dd\", null)"
                        + $", DocumentType = DocumentTypeEnum.{meta.DocumentType.ToString()}"
                        + $", RelativePath = \"{document.RelativePath}\" }},");
            }

            this.offset.OffsetNumber--;
            this.AppendWithOffset(@"};");

            this.offset.OffsetNumber--;
            this.AppendWithOffset(@"}");

            this.offset.OffsetNumber--;
            this.AppendWithOffset(@"}");
        }

        /// <summary>
        /// The document index.
        /// </summary>
        public StringBuilder DocumentIndex => this.stringBuilder;

        private void AppendWithOffset(string data)
        {
            this.AppendWithOffset(this.stringBuilder, this.offset, data);
        }

        private void AppendWithOffset()
        {
            this.AppendWithOffset(this.stringBuilder, this.offset);
        }

        private void AppendWithOffset(StringBuilder builder, OffsetInfo textOffset)
        {
            builder.Append(textOffset.Offset);
            builder.Append(Environment.NewLine);
        }

        private void AppendWithOffset(StringBuilder builder, OffsetInfo textOffset, string data)
        {
            builder.Append(textOffset);
            builder.Append(data);
            builder.Append(Environment.NewLine);
        }
    }
}