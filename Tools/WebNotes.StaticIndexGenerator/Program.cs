﻿namespace WebNotes.StaticIndexGenerator
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    using WebNotes.Document.Domain;
    using WebNotes.Document.Model;

    class Program
    {
        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        static int Main(string[] args)
        {
            try
            {
                Console.WriteLine($"Hello from {Assembly.GetExecutingAssembly().GetName()}");
                Console.WriteLine($"Current directory: {Environment.CurrentDirectory}");

                var rootDirectory = Path.Combine(Environment.CurrentDirectory,"../../", "WebNotesView");
                Console.WriteLine($"Expecting root dir at: {rootDirectory}");

                var rootInfo = new DirectoryInfo(rootDirectory);
                if (!rootInfo.Exists)
                {
                    Console.WriteLine("Root directory doesn't exist.");
                    return 2;
                }

                var targetDirectory = Path.Combine(rootInfo.FullName, "Generated");
                Console.WriteLine($"Expecting generated dir at: {targetDirectory}");

                var documentsDirectory = Path.Combine(rootInfo.FullName, "wwwroot", "Documents");
                Console.WriteLine($"Expecting documents dir at: {documentsDirectory}");

                var targetInfo = new DirectoryInfo(targetDirectory);
                if (targetInfo.Exists)
                {
                    Console.WriteLine("Target directory exists. Continue to generate content.");
                }
                else
                {
                    targetInfo.Create();
                }

                var documentsDir = new DirectoryInfo(documentsDirectory);
                if (documentsDir.Exists)
                {
                    var allDocs = ReadDocuments(documentsDir);
                    var builder = new DocumentIndexBuilder(allDocs);
                    var generatedText = builder.DocumentIndex.ToString();

                    var targetFilePath = Path.Combine(targetInfo.FullName, "DocumentIndex.cs");
                    Console.WriteLine($"Writing down index file: {targetFilePath}");

                    File.WriteAllText(targetFilePath, generatedText.ToString(), Encoding.Default);
                }
                else
                {
                    Console.WriteLine("Documents directory not found. Quiting...");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("***");
                Console.WriteLine("Unhandled exception.");
                Console.WriteLine(exception.Message);
                return 1;
            }

            return 0;
        }

        private static LinkedList<DocumentEntity> ReadDocuments(DirectoryInfo documentsDir)
        {
            var allDocs = new LinkedList<DocumentEntity>();
            Console.WriteLine("Documents directory found. Reading documents...");
            var allDocuments = documentsDir.GetFiles("*.md", SearchOption.AllDirectories);
            var parser = new DocumentReader();
            Console.WriteLine($"Found {allDocuments.Length} documents.");
            if (allDocuments.Any())
            {
                var rootUri = new Uri(documentsDir.FullName);
                foreach (var item in allDocuments)
                {
                    Console.WriteLine($"Processing {item.Name}");
                    var parsedDoc = parser.Read(File.ReadAllText(item.FullName));
                    var relativePath = rootUri.MakeRelativeUri(new Uri(item.FullName));
                    Console.WriteLine($"Calculated relative path: {relativePath}");
                    parsedDoc.RelativePath = relativePath.ToString();
                    allDocs.AddLast(parsedDoc);
                }
            }

            return allDocs;
        }
    }
}
