﻿using System;
using WebNotes.Document.Model;
using YamlDotNet.Serialization;

namespace WebNotes.Document.Domain
{
    public class DocumentReader
    {
        public Model.DocumentEntity Read(string rawInput)
        {
            if (!rawInput.StartsWith("@{"))
            {
                throw new ApplicationException("DocumentEntity's metadata not present or of unsuported format. Metadata opening block not found.");
            }

            var metaEndIndex = rawInput.IndexOf("}@", StringComparison.OrdinalIgnoreCase);
            if (metaEndIndex < 0)
            {
                throw new ApplicationException("DocumentEntity's metadata not present or of unsuported format. Metadata closing block not found.");
            }

            //var metadataText = rawInput[2..metaEndIndex].Trim();
            var metadataText = rawInput.Substring(2, metaEndIndex -2).Trim();
            if (string.IsNullOrEmpty(metadataText))
            {
                throw new ApplicationException("Metadata text is empty.");
            }

            var deserializer = new DeserializerBuilder().Build();
            var metadata = deserializer.Deserialize<Metadata>(metadataText);

            var contentIndex = metaEndIndex + 2;
            var contentBody = rawInput.Substring(contentIndex).Trim();

            var result = new DocumentEntity();
            result.Metadata = metadata;
            result.Content = contentBody;
            return result;
        }
    }
}