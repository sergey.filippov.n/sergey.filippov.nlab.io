﻿namespace WebNotes.Document.Model
{
    using System;

    /// <summary>
    /// The index metadata.
    /// </summary>
    public class IndexMetadata : Metadata, IIndexMetadata
    {
        /// <summary>
        /// Gets or sets the relative path.
        /// </summary>
        public string RelativePath { get; set; }

        /// <summary>
        /// Gets or sets the document id.
        /// </summary>
        public Guid DocumentId { get; set; }
    }
}