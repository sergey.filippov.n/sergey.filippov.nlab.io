﻿namespace WebNotes.Document.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class DocumentEntity
    {
        public Metadata Metadata { get; set; }

        public string Content { get; set; }

        [NotMapped]
        public string RelativePath { get; set; }
    }
}