﻿namespace WebNotes.Document.Model
{
    public enum DocumentTypeEnum
    {
        /// <summary>
        /// The blog entry.
        /// </summary>
        BlogEntry,

        /// <summary>
        /// The app entry.
        /// </summary>
        AppEntry
    }
}