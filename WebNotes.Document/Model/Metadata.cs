﻿namespace WebNotes.Document.Model
{
    using System;

    /// <summary>
    /// The metadata.
    /// </summary>
    public class Metadata
    {
        /// <summary>
        /// Gets or sets the document type.
        /// </summary>
        public DocumentTypeEnum DocumentType { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the link.
        /// </summary>
        public string Link { get; set; }
    }
}