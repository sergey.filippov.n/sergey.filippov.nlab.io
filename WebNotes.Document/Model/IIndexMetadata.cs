﻿namespace WebNotes.Document.Model
{
    using System;

    public interface IIndexMetadata
    {
        /// <summary>
        /// Gets the relative path.
        /// </summary>
        string RelativePath { get; }

        /// <summary>
        /// Gets or sets the document id.
        /// </summary>
        Guid DocumentId { get; set; }

        /// <summary>
        /// Gets the document type.
        /// </summary>
        DocumentTypeEnum DocumentType { get; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the author.
        /// </summary>
        string Author { get; }

        /// <summary>
        /// Gets the date.
        /// </summary>
        DateTime Date { get; }

        /// <summary>
        /// Gets the link.
        /// </summary>
        string Link { get; }
    }
}