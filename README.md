# Web notes
## Blazor based purely client side web application

This is a simple example of what can be done usin MS Blazor (dotnet running on
WASM inside of the web browser).

### Goals
The current experimental implementation is aiming to achieve a state, when
we can easely modify MD files in repository and get properly working staticly
compiled web site, that has all the repository changes auto generated and 
included into pages index.

### How it works
To achieve this, currently custom build target is used from inside of the root
project, to analyze predefined directory structure and generate a simple *.cs 
file with metadata about existing "documents" (md files to be rendered).

### How can I give it a try?
This is fairly simple, just make sure you have dotnet core 3 preview 8 or later
installed.
Then

`dotnet publish -c Release -o bin/pub`

or just 

`dotnet run` 

from the root of the project.