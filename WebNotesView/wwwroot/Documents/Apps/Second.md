﻿@{DocumentType: AppEntry
Name: The second entry.
Author: Sergey Filippov
Date: 2019-08-21
Link: https://google.com}@

# The app entry.
The content to be rendered as a card with a link.