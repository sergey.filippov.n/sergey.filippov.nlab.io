@{DocumentType: BlogEntry
Name: Performance optimization.
Author: Sergey Filippov
Date: 2019-08-27}@

## Performance optimization
As result of small investigation, there are some critical factors which were causing serious performance problems with existing solution:

* The latest dotnet core 3 preview 8 has a bug in `mono.wasm` and this causing linking of .net standard 2.1 to faile laters execution of client side
* Disabling of liking on other hand causing massive growth of file sizes and general overhead

As result, total download size of a site went down from 16 to 6 mb. I think this is a big difference.

### Goodbye Radzen
By some reason, [**Radzen**](https://blazor.radzen.com/) is recomending disabling of linking in order to avoid problems and that is not a solution I'm ready to give a try.
For this reason I've desided to move to some less requiring component framework. [**Blazorstrap**](https://blazorstrap.io/) were chosen as a drop-in replacement.

So far it's strictly positive experiense.

For example, this is how one can change themes now:


```csharp
protected override async Task OnAfterRenderAsync()
    {
        if (!this.hasBeendone)
        {
            await BootstrapCSS.SetBootstrapCSS(Theme.Darkly, "4.3.1");
            this.hasBeendone = true;
        }
    }
```