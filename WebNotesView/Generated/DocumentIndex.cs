namespace WebNotesView.Generated
{
    using System;
    using WebNotes.Document.Model;
    
    /// <summary>
    /// The document index.
    /// </summary>
    public class DocumentIndex
    {
        /// <summary>
        /// The metadata index.
        /// </summary>
        public readonly IIndexMetadata[] MetadataIndex =
        {
            new IndexMetadata { Name = "The first entry.", Author = "Sergey Filippov", Date = DateTime.ParseExact("2019-08-18", "yyyy-MM-dd", null), DocumentType = DocumentTypeEnum.BlogEntry, RelativePath = "Documents/First.md" },
            new IndexMetadata { Name = "Performance optimization.", Author = "Sergey Filippov", Date = DateTime.ParseExact("2019-08-27", "yyyy-MM-dd", null), DocumentType = DocumentTypeEnum.BlogEntry, RelativePath = "Documents/mdTest.md" },
            new IndexMetadata { Name = "The second entry.", Author = "Sergey Filippov", Date = DateTime.ParseExact("2019-08-21", "yyyy-MM-dd", null), DocumentType = DocumentTypeEnum.AppEntry, RelativePath = "Documents/Apps/Second.md" },
        };
    }
}
