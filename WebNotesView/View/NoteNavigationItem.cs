﻿namespace WebNotesView.View
{
    public class NoteNavigationItem
    {
        public string Name { get; set; }

        public string Icon { get; set; }

        public string Path { get; set; }
    }
}