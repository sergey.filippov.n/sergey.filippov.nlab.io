﻿namespace WebNotesView.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using WebNotes.Document.Model;

    using WebNotesView.Generated;

    /// <summary>
    /// The document storage.
    /// </summary>
    public class DocumentStorage
    {
        private readonly DocumentTypeEnum storageType;

        private readonly Dictionary<Guid, DocumentEntity> documentIndex = new Dictionary<Guid, DocumentEntity>();

        private Dictionary<Guid, IIndexMetadata> metaIndex = new Dictionary<Guid, IIndexMetadata>();

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentStorage"/> class. 
        /// </summary>
        /// <param name="storageType">
        /// Type of documents stored.
        /// </param>
        /// <param name="rawIndex">
        /// The raw index.
        /// </param>
        public DocumentStorage(DocumentTypeEnum storageType, DocumentIndex rawIndex)
        {
            this.storageType = storageType;
            this.InitializeIndex(rawIndex);
        }

        /// <summary>
        /// The meta.
        /// </summary>
        public IReadOnlyDictionary<Guid, IIndexMetadata> Meta => this.metaIndex;

        /// <summary>
        /// The documents.
        /// </summary>
        public IReadOnlyDictionary<Guid, DocumentEntity> Documents => this.documentIndex;

        /// <summary>
        /// The add document.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="document">
        /// The document.
        /// </param>
        public void AddDocument(Guid id, DocumentEntity document)
        {
            if (this.documentIndex.ContainsKey(id))
            {
                this.documentIndex[id] = document;
            }
            else
            {
                this.documentIndex.Add(id, document);
            }
        }

        private void InitializeIndex(DocumentIndex rawIndex)
        {
            this.metaIndex = rawIndex.MetadataIndex.Where(item => item.DocumentType == this.storageType)
                .OrderByDescending(item => item.Date).ToDictionary(key => key.DocumentId, item => item);
        }
    }
}