﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using WebNotesView.Services;
using WebNotesView.View;

namespace WebNotesView.Shared
{
    public class NavigationMenuBase : ComponentBase
    {
        [Inject]
        public IMenuItemsProviderService MenuProvider { private get; set; }

        [Parameter]
        public bool IsDocked { get; set; }
        
        public IReadOnlyCollection<NoteNavigationItem> MenuItems { get; private set; }

        protected override async Task OnInitializedAsync()
        {
            var items = await MenuProvider.GetMainMenuItemsAsync();
            this.MenuItems = items;
        }

        public async Task<IReadOnlyCollection<NoteNavigationItem>> GetMenuItems()
        {
            var allItems = await this.MenuProvider.GetMainMenuItemsAsync();
            return allItems;
        }
    }
}