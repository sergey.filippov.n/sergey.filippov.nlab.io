﻿namespace WebNotesView.Shared
{
    using System.Threading.Tasks;

    using BlazorStrap;

    using Microsoft.AspNetCore.Components;

    public class FlexLayoutBase : MainLayoutBase
    {
        [Inject] public IBootstrapCSS BootstrapCSS {get;set;}

        [Inject] public NavigationManager NavigationManager {get;set;}

        private Theme currentTheme = Theme.Darkly;

        private bool hasBeenDone;

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!this.hasBeenDone)
            {
                await this.ApplyTheme();
            }
        }

        protected async Task ToggleTheme()
        {
            if (currentTheme == Theme.Darkly)
            {
                this.currentTheme = Theme.Bootstrap;
            }
            else
            {
                this.currentTheme = Theme.Darkly;
            }

            await this.ApplyTheme();
            this.StateHasChanged();
        }

        private async Task ApplyTheme()
        {
            await BootstrapCSS.SetBootstrapCSS(this.currentTheme, "4.3.1");
            this.hasBeenDone = true;
        }
    }
}