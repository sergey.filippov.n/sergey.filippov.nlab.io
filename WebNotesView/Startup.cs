using Microsoft.AspNetCore.Components.Builder;
using Microsoft.Extensions.DependencyInjection;
using WebNotesView.Services;
using BlazorStrap;

namespace WebNotesView
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddBootstrapCSS();

            services.AddScoped<StateService>();

            services.AddSingleton<IMenuItemsProviderService, MenuItemsProviderService>();
            services.AddSingleton<IDocumentProviderService, DocumentProviderService>();
            services.AddSingleton<IMarkdownRenderService, MarkdownRenderService>();
        }

        public void Configure(IComponentsApplicationBuilder app)
        {
            app.AddComponent<App>("app");
        }
    }
}
