﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebNotesView.View;

namespace WebNotesView.Services
{
    using BlazorStrap;

    public interface IMenuItemsProviderService
    {
        Task<IReadOnlyCollection<NoteNavigationItem>> GetMainMenuItemsAsync();
    }

    public class MenuItemsProviderService : IMenuItemsProviderService
    {
        public Task<IReadOnlyCollection<NoteNavigationItem>> GetMainMenuItemsAsync()
        {
            var result = new LinkedList<NoteNavigationItem>();
            result.AddLast(new NoteNavigationItem() { Name = "Home", Path = "/", Icon = "home" });
            result.AddLast(new NoteNavigationItem() { Name = "Editor", Path = "/editor", Icon = "aspect_ratio" });
            //result.AddLast(new NoteNavigationItem() { Name = "Fetch data", Path = "/fetchdata", Icon = "grid_on" });

            return Task.FromResult(result as IReadOnlyCollection<NoteNavigationItem>);
        }
    }
}