﻿namespace WebNotesView.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Net.Http;
    using System.Threading.Tasks;

    using WebNotes.Document.Domain;
    using WebNotes.Document.Model;

    using WebNotesView.Domain;
    using WebNotesView.Generated;

    public interface IDocumentProviderService
    {
        /// <summary>
        /// The get document by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task{DocumentEntity}"/>.
        /// </returns>
        Task<DocumentEntity> GetBlogById(Guid id);

        /// <summary>
        /// The get app by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<DocumentEntity> GetAppById(Guid id);

        /// <summary>
        /// The get blog documents.
        /// </summary>
        /// <param name="take">
        /// The take.
        /// </param>
        /// <param name="skip">
        /// The skip.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<DocumentEntity>> GetBlogs(int? take = null, int? skip = null);

        /// <summary>
        /// The get app documents.
        /// </summary>
        /// <param name="take">
        /// The take.
        /// </param>
        /// <param name="skip">
        /// The skip.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<DocumentEntity>> GetApps(int? take = null, int? skip = null);

        /// <summary>
        /// The get all apps.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<DocumentEntity>> GetAllApps();

        /// <summary>
        /// The get all blogs.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<DocumentEntity>> GetAllBlogs();
    }

    /// <summary>
    /// The document provider service.
    /// </summary>
    public class DocumentProviderService : IDocumentProviderService
    {
        private readonly HttpClient httpClient;
        private readonly DocumentReader reader = new DocumentReader();

        private DocumentStorage blogStorage;
        private DocumentStorage appStorage;

        public DocumentProviderService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
            this.BuildIndex(new DocumentIndex());
        }

        /// <summary>
        /// The get document by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task{DocumentEntity}"/>.
        /// </returns>
        public async Task<DocumentEntity> GetBlogById(Guid id)
        {
            var storage = this.blogStorage;
            return await this.GetDocument(storage, id);
        }

        /// <summary>
        /// The get app by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<DocumentEntity> GetAppById(Guid id)
        {
            var storage = this.blogStorage;
            return await this.GetDocument(storage, id);
        }

        /// <summary>
        /// The get blog documents.
        /// </summary>
        /// <param name="take">
        /// The take.
        /// </param>
        /// <param name="skip">
        /// The skip.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<DocumentEntity>> GetBlogs(int? take = null, int? skip = null)
        {
            var storage = this.blogStorage;
            return await this.GetDocuments(storage, take, skip);
        }

        /// <summary>
        /// The get app documents.
        /// </summary>
        /// <param name="take">
        /// The take.
        /// </param>
        /// <param name="skip">
        /// The skip.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<DocumentEntity>> GetApps(int? take = null, int? skip = null)
        {
            var storage = this.appStorage;
            return await this.GetDocuments(storage, take, skip);
        }

        /// <summary>
        /// The get all apps.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<DocumentEntity>> GetAllApps()
        {
            return await this.GetDocuments(this.appStorage, this.appStorage.Meta.Count);
        }

        /// <summary>
        /// The get all blogs.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<DocumentEntity>> GetAllBlogs()
        {
            return await this.GetDocuments(this.blogStorage, this.blogStorage.Meta.Count);
        }

        /// <summary>
        /// The get documents.
        /// </summary>
        /// <param name="storage">
        /// The storage.
        /// </param>
        /// <param name="take">
        /// The take.
        /// </param>
        /// <param name="skip">
        /// The skip.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<IEnumerable<DocumentEntity>> GetDocuments(DocumentStorage storage, int? take = null, int? skip = null)
        {
            List<IIndexMetadata> metasToProcess;

            if (skip == null)
            {
                skip = 0;
            }

            if (take == null)
            {
                take = 10;
            }

            if (storage.Meta.Count < skip)
            {
                metasToProcess = new List<IIndexMetadata>();
            }
            else if (storage.Meta.Count < take)
            {
                metasToProcess = new List<IIndexMetadata>(this.blogStorage.Meta.Values);
            }
            else if (skip + take > storage.Meta.Count)
            {
                metasToProcess = new List<IIndexMetadata>(this.blogStorage.Meta.Values.Skip(skip.Value));
            }
            else
            {
                metasToProcess = new List<IIndexMetadata>(this.blogStorage.Meta.Values.Skip(skip.Value).Take(take.Value));
            }

            var result = new LinkedList<DocumentEntity>();

            foreach (var metadata in metasToProcess)
            {
                var document = await this.GetDocument(storage, metadata.DocumentId);
                result.AddLast(document);
            }

            return result;
        }

        /// <summary>
        /// The get document.
        /// </summary>
        /// <param name="storage">
        /// The storage.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task{DocumentEntity}"/>.
        /// </returns>
        private async Task<DocumentEntity> GetDocument(DocumentStorage storage, Guid id)
        {
            if (storage.Meta.TryGetValue(id, out var meta))
            {
                if (storage.Documents.TryGetValue(id, out var docIndex))
                {
                    return docIndex;
                }

                var rawDocument = await this.httpClient.GetStringAsync(meta.RelativePath);
                var document = this.reader.Read(rawDocument);
                storage.AddDocument(id, document);

                return document;
            }

            return null;
        }

        private void BuildIndex(DocumentIndex documentIndex)
        {
            foreach (var item in documentIndex.MetadataIndex)
            {
                item.DocumentId = Guid.NewGuid();
            }

            this.appStorage = new DocumentStorage(DocumentTypeEnum.AppEntry, documentIndex);
            this.blogStorage = new DocumentStorage(DocumentTypeEnum.BlogEntry, documentIndex);
        }
    }
}