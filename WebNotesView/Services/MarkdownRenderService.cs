﻿namespace WebNotesView.Services
{
    using Markdig;

    using Microsoft.AspNetCore.Components;

    public interface IMarkdownRenderService
    {
        /// <summary>
        /// The render.
        /// </summary>
        /// <param name="rawMarkdown">
        /// The raw markdown.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        MarkupString Render(string rawMarkdown);
    }

    /// <summary>
    /// The markdown render service.
    /// </summary>
    public class MarkdownRenderService : IMarkdownRenderService
    {
        private readonly MarkdownPipeline pipeline;

        /// <summary>
        /// Initializes a new instance of the <see cref="MarkdownRenderService"/> class.
        /// </summary>
        public MarkdownRenderService()
        {
            //this.pipeline = new MarkdownPipelineBuilder().UseAdvancedExtensions().UseBootstrap().UseMediaLinks()
            //    .UseEmojiAndSmiley().Build();

            this.pipeline = new MarkdownPipelineBuilder().UseAdvancedExtensions().UseEmojiAndSmiley().Build();
        }

        /// <summary>
        /// The render.
        /// </summary>
        /// <param name="rawMarkdown">
        /// The raw markdown.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public MarkupString Render(string rawMarkdown)
        {
            return (MarkupString)Markdown.ToHtml(rawMarkdown, this.pipeline);
        }
    }
}
