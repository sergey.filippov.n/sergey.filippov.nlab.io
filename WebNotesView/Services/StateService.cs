﻿namespace WebNotesView.Services
{
    using System.Collections.Generic;

    /// <summary>
    /// The state service.
    /// </summary>
    public class StateService
    {
        private readonly List<string> supportedThemes = new List<string> { "humanistic", "darkCode" };

        /// <summary>
        /// Initializes a new instance of the <see cref="StateService"/> class.
        /// </summary>
        public StateService()
        {
            this.ThemeName = "darkCode";
        }

        /// <summary>
        /// Gets or sets the theme name.
        /// </summary>
        public string ThemeName { get; set; }
    }
}